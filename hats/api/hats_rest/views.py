from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from common.json import ModelEncoder
import json

from .acl import get_photo
from .models import LocationVO, Hat


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = ["import_href", "closet_name", "section_number", "shelf_number"]


class HatsListEncoder(ModelEncoder):
    model = Hat
    properties = ["id", "color", "fabric", "style_name", "picture_url"]


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = ["id", "color", "fabric", "style_name", "picture_url", "location"]

    encoders = {"location": LocationVOEncoder()}


@require_http_methods(["GET", "POST"])
def api_hats(request):
    if request.method == "GET":
        hats = Hat.objects.all()

        return JsonResponse({"hats": hats}, encoder=HatsListEncoder)
    else:
        content = json.loads(request.body)

        try:
            location = LocationVO.objects.get(id=content["location"])
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse({"message": "Invalid Location ID"}, status=400)

        approved_colors = [
            "red",
            "orange",
            "yellow",
            "green",
            "turquoise",
            "blue",
            "violet",
            "pink",
            "brown",
            "black",
            "gray",
            "white",
        ]

        color = content["color"].lower().strip()

        if color in approved_colors:
            content["picture_url"] = get_photo(color, content["style_name"])
        else:
            content["picture_url"] = get_photo("black", content["style_name"])

        hat = Hat.objects.create(**content)
        return JsonResponse(hat, encoder=HatDetailEncoder, safe=False)


@require_http_methods(["GET", "PUT", "DELETE"])
def api_hat(request, id):
    if request.method == "GET":
        hat = Hat.objects.get(id=id)

        return JsonResponse(hat, encoder=HatDetailEncoder, safe=False)
    elif request.method == "DELETE":
        count, items = Hat.objects.filter(id=id).delete()

        if count > 0:
            return JsonResponse({"deleted": {"successful": count > 0, "items": items}})
        else:
            return JsonResponse(
                {
                    "deleted": {
                        "successful": count > 0,
                        "message": "Hat could not be deleted. Either invalid shoe ID or shoe has already been deleted.",
                    }
                }
            )
    else:
        content = json.loads(request.body)

        if "location" in content:
            try:
                location = LocationVO.objects.get(id=content["location"])
                content["location"] = location
            except LocationVO.DoesNotExist:
                return JsonResponse({"message": "Invalid Location ID"}, status=400)

        approved_colors = [
            "red",
            "orange",
            "yellow",
            "green",
            "turquoise",
            "blue",
            "violet",
            "pink",
            "brown",
            "black",
            "gray",
            "white",
        ]

        color = content["color"].lower().strip()

        if color in approved_colors:
            content["picture_url"] = get_photo(color, content["style_name"])
        else:
            content["picture_url"] = get_photo("black", content["style_name"])

        Hat.objects.filter(id=id).update(**content)
        hat = Hat.objects.get(id=id)

        return JsonResponse(hat, encoder=HatDetailEncoder, safe=False)

import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './components/App';
import './index.css'

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

async function loadHatsAndShoes() {
  try {

    const requests = [];

    const hatsURL = 'http://localhost:8090/api/hats/'
    const shoesURL = 'http://localhost:8080/api/shoes/'
    requests.push(fetch(hatsURL))
    requests.push(fetch(shoesURL))

    const responses = await Promise.all(requests)

    if (responses[0].ok && responses[1].ok) {
      const hatsData = await responses[0].json();
      const shoesData = await responses[1].json();

      root.render(
        <React.StrictMode>
          <App hats={hatsData.hats} shoes={shoesData.shoes}/>
        </React.StrictMode>
      )
    } else {
      console.error(responses[0], responses[1]);
    }
  } catch (e) {
    console.log(e)
  }
}

loadHatsAndShoes();

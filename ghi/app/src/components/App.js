import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatForm from './hats/HatForm';
import HatList from './hats/HatList';
import HatDetail from './hats/HatDetail';
import ShoeForm from './shoes/ShoeForm';
import ShoeList from './shoes/ShoeList';
import ShoeDetail from './shoes/ShoeDetail';


function App(props) {
  if ((props.hats === undefined) || (props.shoes === undefined)) {
    return null
  }

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route index element={<HatList hats={props.hats}/>}/>
            <Route path="new" element={<HatForm/>}/>
            <Route path=":id" element={<HatDetail/>}/>
          </Route>
          <Route path="shoes">
            <Route index element={<ShoeList shoes={props.shoes}/>}/>
            <Route path="new" element={<ShoeForm/>}/>
            <Route path=":id" element={<ShoeDetail/>}/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;

import requests
from .keys import PEXELS_KEY


def get_photo(color, manufacturer, model_name):
    params = {
        "query": f"{color} {manufacturer} {model_name}",
        "per_page": 3,
    }

    headers = {"Authorization": PEXELS_KEY}
    url = "https://api.pexels.com/v1/search"

    api_response = requests.get(url, params=params, headers=headers)

    try:
        return api_response.json()["photos"][0]["url"]
    except KeyError or IndexError:
        return None

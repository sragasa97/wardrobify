# Generated by Django 4.0.3 on 2023-03-05 07:00

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BinVO',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('closet_name', models.CharField(max_length=100)),
                ('bin_number', models.PositiveSmallIntegerField()),
                ('bin_size', models.PositiveSmallIntegerField()),
                ('import_href', models.CharField(max_length=150)),
            ],
            options={
                'ordering': ('closet_name', 'bin_number', 'bin_size'),
            },
        ),
        migrations.CreateModel(
            name='Shoe',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('manufacturer', models.CharField(max_length=150)),
                ('model_name', models.CharField(max_length=150)),
                ('color', models.CharField(max_length=50)),
                ('picture_url', models.URLField(null=True)),
                ('bin', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='shoe', to='shoes_rest.binvo')),
            ],
            options={
                'ordering': ('color', 'manufacturer', 'model_name'),
            },
        ),
    ]

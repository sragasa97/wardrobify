from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from common.json import ModelEncoder
import json

from .acl import get_photo
from .models import BinVO, Shoe


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = ["import_href", "closet_name", "bin_number", "bin_size"]


class ShoesListEncoder(ModelEncoder):
    model = Shoe
    properties = ["id", "color", "manufacturer", "model_name", "picture_url"]


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = ["id", "color", "manufacturer", "model_name", "picture_url", "bin"]

    encoders = {"bin": BinVOEncoder()}


@require_http_methods(["GET", "POST"])
def api_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()

        return JsonResponse({"shoes": shoes}, encoder=ShoesListEncoder)
    else:
        content = json.loads(request.body)

        try:
            bin = BinVO.objects.get(id=content["bin"])
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse({"message": "Invalid Bin ID"}, status=400)

        approved_colors = [
            "red",
            "orange",
            "yellow",
            "green",
            "turquoise",
            "blue",
            "violet",
            "pink",
            "brown",
            "black",
            "gray",
            "white",
        ]

        color = content["color"].lower().strip()

        if color in approved_colors:
            content["picture_url"] = get_photo(
                color, content["manufacturer"], content["model_name"]
            )
        else:
            content["picture_url"] = get_photo(
                "black", content["manufacturer"], content["model_name"]
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(shoe, encoder=ShoeDetailEncoder, safe=False)


@require_http_methods(["GET", "PUT", "DELETE"])
def api_shoe(request, id):
    if request.method == "GET":
        shoe = Shoe.objects.get(id=id)

        return JsonResponse(shoe, encoder=ShoeDetailEncoder, safe=False)
    elif request.method == "DELETE":
        count, items = Shoe.objects.filter(id=id).delete()

        if count > 0:
            return JsonResponse({"deleted": {"successful": count > 0, "items": items}})
        else:
            return JsonResponse(
                {
                    "deleted": {
                        "successful": count > 0,
                        "message": "Shoe could not be deleted. Either invalid shoe ID or shoe has already been deleted.",
                    }
                }
            )
    else:
        content = json.loads(request.body)

        if "bin" in content:
            try:
                bin = BinVO.objects.get(id=content["bin"])
                content["bin"] = bin
            except BinVO.DoesNotExist:
                return JsonResponse({"message": "Invalid Bin ID"}, status=400)

        approved_colors = [
            "red",
            "orange",
            "yellow",
            "green",
            "turquoise",
            "blue",
            "violet",
            "pink",
            "brown",
            "black",
            "gray",
            "white",
        ]

        color = content["color"].lower().strip()

        if color in approved_colors:
            content["picture_url"] = get_photo(
                color, content["manufacturer"], content["model_name"]
            )
        else:
            content["picture_url"] = get_photo(
                "black", content["manufacturer"], content["model_name"]
            )

        Shoe.objects.filter(id=id).update(**content)
        shoe = Shoe.objects.get(id=id)

        return JsonResponse(shoe, encoder=ShoeDetailEncoder, safe=False)
